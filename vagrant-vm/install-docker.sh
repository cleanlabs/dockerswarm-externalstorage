#!/bin/bash
sudo yum update -y 
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io -y 
sudo systemctl start docker
sudo chown vagrant:vagrant /var/run/docker.sock	
